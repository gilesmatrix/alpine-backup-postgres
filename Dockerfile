FROM python:3-alpine

MAINTAINER Brenton Bills <Brenton.Bills@gmail.com>

ENV HOME=/usr/src/app
ENV PATH=$HOME:$PATH

RUN echo "@edge http://nl.alpinelinux.org/alpine/edge/main" >> /etc/apk/repositories && \
apk update && apk add curl "postgresql@edge<9.6" && \
pip install dropbox

COPY cron /var/spool/cron/crontabs/root
COPY backup $HOME/backup
COPY pgpass $HOME/.pgpass
RUN chmod 0600 $HOME/.pgpass

WORKDIR $HOME

CMD crond -l 2 -f
